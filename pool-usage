#!/bin/sh

# pool-usage -- Get the storage usage of a ZFS pool.
# Usage: pool-usage pool-name

# Copyright 2021 Michael Fiano
#
# Redistribution and use in source and binary forms, with or without modification, are permitted
# provided that the following conditions are met:
#
#   1. Redistributions of source code must retain the above copyright notice, this list of
#      conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright notice, this list of
#      conditions and the following disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

print_usage() {
  echo "Usage: pool-usage pool-name"
}

parse_args() {
  if [ "$1" ]; then
    opt_pool=$1
  else
    print_usage
    exit 1
  fi
}

parse_args "$@"

stats=$(zpool list -H -o alloc,size "$opt_pool" 2> /dev/null)
if [ "$stats" ]; then
  used=$(echo "$stats" | cut -w -f 1)
  total=$(echo "$stats" | cut -w -f 2)
  echo "Used: $used"
  echo "Total: $total"
else
  echo "Pool '$opt_pool' not found."
  exit 1
fi
